<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatBusiness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('business', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->integer('max_level');
            $table->integer('max_workers');
            $table->float('profit');
        });

        DB::table('business')->insert(
            ['name' => 'Пиццерия', 'price' => 140,'max_level'=>5,'max_workers'=>10,'profit'=>2],
            ['name' => 'Клуб', 'price' => 500,'max_level'=>10,'max_workers'=>15,'profit'=>5],
            ['name' => 'Кафе', 'price' => 1500,'max_level'=>15,'max_workers'=>25,'profit'=>20],
            ['name' => 'Завод', 'price' => 2000,'max_level'=>20,'max_workers'=>30,'profit'=>40],
            ['name' => 'Супермаркет', 'price' => 5000,'max_level'=>25,'max_workers'=>40,'profit'=>50]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::dropIfExists('business');
    }
}
