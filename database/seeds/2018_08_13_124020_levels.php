<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Levels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('experience');

        });
        DB::table('levels')->insert([
           ['experience'=>30],['experience'=>60],['experience'=>120],['experience'=>30],['experience'=>150],['experience'=>200],['experience'=>250],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('levels');
    }
}
