<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PfofitBusinessUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
        /**
         * @param Blueprint $table
         */
            'profit_business_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->float ('profit');
            $table->integer ('last_update');
            /** @var TYPE_NAME $table */
            $table->timestamp ( 'updated_at');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists ('profit_business_users');
    }
}
