<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Level;
use App\Http\Controllers\Users;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new Users(Auth::id());
    }

    /**
     *
     */
    public function  level (){
        /** @var TYPE_NAME $countLevels */
        $countLevels = Level::select( 'id')->count();//Count levels
        /** @var TYPE_NAME $needExp */
        $needExp = Level::select( 'experience')->where( 'id', Auth::user()->level)->first();// How much experience need fro next level
        /** @var TYPE_NAME $bonusMoney */
        $bonusMoney= Auth::user()->level*50;//Reward money for up level

        /** @var TYPE_NAME $countLevels */
        if($countLevels > Auth::user()->level) {

            if (Auth::user ()->experience < $needExp->experience) {
            } else {
                User::where ( 'id' , Auth::id () )->update ( ['level' => Auth::user ()->level + 1 , 'money' => Auth::user ()->money + $bonusMoney , 'experience' => Auth::user ()->experience - $needExp->experience] );

            }

        }


    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function  view(){
        return view('profile');
    }

    public function  last_action(){

        return  User::where('id',Auth::id())->update(['last_action'=>time (), 'ip'=>$_SERVER['REMOTE_ADDR']]);

    }

    /**
     * @return int
     */
    public  function  get(){
        var_dump ($this->user->getLevel ());
    }

}
