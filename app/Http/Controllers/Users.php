<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class Users extends Controller
{

    /**
     * Users constructor.
     * @param $userID
     */
    public function __construct($userID)
    {
        $this->userID=$userID;
    }

    /**
     * @return mixed
     */
    public  function  getIDUser(){

        return $this->userID;
    }

    /**
     * @param $column
     * @return mixed
     */
    public function getInfo ($column){
        /** @var TYPE_NAME $result */
        $result = User::select ( $column)->where ( 'id' , $this->userID)->first ();
        return $result->$column;
}
    /**
     * @return mixed
     */
    public function getLevel ()
    {
        return $this->getInfo ('level');
    }

    /**
     * @return money user
     */
    public function getMoney(){
        return $this->getInfo ('money');

    }


}
