<?php

namespace App\Http\Controllers;

use App\ProfitBusinessUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\BusinessUser;
use App\Business;
use App\User;
use App\Http\Controllers\Users;


class BusinessController extends Controller
{
    public $priceUpgradeBusiness=array(50,100,250,500,950,1500,2500,4000,6000,10000,15000);
    public $priceHireWorkers=array(50,100,250,500,950,1500,2500,4000,6000,10000,15000);



    /**
     * BusinessController constructor.
     */
    public function __construct()
    {

        $this->countBusiness=Business::select('id')->count();
        $countBusinessUser= BusinessUser::select('id')->where('id_user', Auth::id())->count();
        $this->user = new Users(Auth::id());
        $this->profitBusinessUser = ProfitBusinessUser::select()->where('id_user',Auth::id ())->first();

        $this->middleware('auth');


    }
   public function lastActionUser(){
    return $this->profitBusinessUser->last_update;
    }

    //Count business userr
    public function  countBusinesses(){
         $result= BusinessUser::select('id')->where('id_user', Auth::user()->id)->count();
         return $result;
    }

    public  static  function   currentProfitBusiness(){
        return  BusinessUser::select('profit')->where('id_user', Auth::user()->id)->sum('profit');
    }
    //In this method whow all detslies about business
    //Can updagrade business level , new workers
    public  function  detalies($id){
        $business_user=BusinessUser::select()->where(['id'=>$id,'id_user'=>Auth::id()])->first();
       // $countBusiness=BusinessUser::select('id')->where('id_user',Auth::user()->id)->count();

        if(!$business_user){
           return  redirect('/home')->with(['error'=>'Этого бизнеса не существует']);
        }else{
            return view('business.detalies')->with(['business_user' =>$business_user]);
        }

    }
    /*
     * In this method upgrade business ,
     */
    public function upgrade($id){
        $business_user=BusinessUser::select()->where(['id'=>$id,'id_user'=>Auth::id()])->first();

        if(!$business_user){
            return  redirect('/business/'.$id.'/detalies')->with(['error'=>'Этого бизнеса не существует']);
        }elseif ($business_user->max_level <= $business_user->level){
            return   redirect('/business/'.$id.'/detalies')->with(['error'=>'Вы не можете повысить бизнес , так как он максимального уровеня']);

        }elseif ($this->priceUpgradeBusiness[$business_user->level] >  Auth::user()->money ){
            return   redirect('/business/'.$id.'/detalies')->with(['error'=>'Вам не хватает еще '.($this->priceUpgradeBusiness[$business_user->level] -  Auth::user()->money).' чтобы улучшить бизнес']);
        }else{
            $profitUp=($business_user->profit*rand(10,20)/100);
            BusinessUser::where(['id'=>$id,'id_user'=>Auth::id()])->update([
                'level'=>$business_user->level+1,'max_workers'=>$business_user->max_workers+rand(1,5),'profit'=>$business_user->profit+$profitUp]);
            User::where('id',Auth::id())->update(['money'=>Auth::user()->money-$this->priceUpgradeBusiness[$business_user->level]]);

            return  redirect('/business/'.$id.'/detalies')->with(['success'=>'Вы улучшили свой бизнес']);


        }


    }
    /*
    /In this method, we are buying a new business
    /checking whether we have enough money, and also adding to the business_user business
    */
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function  buy()
    {
        $businessInfoBuy = Business::select()->where('id', $this->countBusinesses() + 1)->first();

        //  return Auth::id();
        $countBusiness = Business::select('id')->count();

        if($this->countBusinesses() >= $countBusiness) {
            return redirect('/business')->with(['error' => 'У вас максимальное количество бизнесов']);
        }
     if(Auth::user()->money < $businessInfoBuy->price){
            return redirect('/business')->with(['error_buy'=>'Вам не хватает '.($businessInfoBuy->price-Auth::user()->money).'  чтобы открыть новый бизнес',
                'businessInfoBuy->price'=>$businessInfoBuy->price]);
        }else {
            ProfitBusinessUser::insert(['id_user'=>Auth::id (),'last_update'=>time ()]);
            BusinessUser::insert([ 'id_user'=>Auth::id(), 'id_business'=>$businessInfoBuy->id,
                'profit'=>$businessInfoBuy->profit,'name' => $businessInfoBuy->name,'max_workers'=>$businessInfoBuy->max_workers,'max_level'=>$businessInfoBuy->max_level   ]);
            User::where('id',Auth::id())->update(['money'=>Auth::user()->money-$businessInfoBuy->price]);
            return redirect('/business')->with('success_buy', "Вы открыли " . $businessInfoBuy->name . " за " . $businessInfoBuy->price);
        }
    }

    /*
     * This method hire workers in business and gives profit
     * Formula 1% to  5 from profit  of business
     */
    public function hireWorkers($id){
        $business_user=BusinessUser::select()->where(['id'=>$id,'id_user'=>Auth::id()])->first();

        if(!$business_user){
            return  redirect('/business/'.$id.'/detalies')->with(['error'=>'Этого бизнеса не существует']);
        }elseif ( $business_user->workers >= $business_user->max_workers){
            return   redirect('/business/'.$id.'/detalies')->with(['error'=>'У вас максимальное количетсво рабочих, повысьте уровень бизнеса чтобы нанять больше рабочих']);

        }elseif ($this->priceHireWorkers[$business_user->level] >  Auth::user()->money ) {
            return redirect('/business/' . $id . '/detalies')->with(['error' => 'Вам не хватает еще ' . ($this->priceUpgradeBusiness[$business_user->level] - Auth::user()->money) . ' на нового работника']);
        }else{
            $profitBonusHireWorkers=($business_user->profit*rand(1,5)/100);
            BusinessUser::where(['id'=>$id,'id_user'=>Auth::id()])->update([
                'workers'=>$business_user->workers+1,'profit'=>$business_user->profit+$profitBonusHireWorkers]);
            User::where('id',Auth::id())->update(['money'=>Auth::user()->money-$this->priceHireWorkers[$business_user->level]]);
            return  redirect('/business/'.$id.'/detalies')->with(['success'=>'Поздравляю, у вас новый работник ']);
        }
    }

    /**
     *
     */
    public  function profitMoneyMinute(){
        $time=60;//60 sec every get profit

        if($this->lastActionUser() < time ()){
            $count=floor ((time ()-$this->lastActionUser() )/$time);
            /** @var TYPE_NAME $count */
            if($count>=1){
                $profitMoney = $this->currentProfitBusiness ()*$count;
                User::where('id',Auth::id ())->update( array('money'=> $profitMoney + Auth::user()->money  ) );
                ProfitBusinessUser::where('id_user',Auth::id ())->update(['last_update'=>time ()]);
                redirect ('?');

            }

        }
    }


}

