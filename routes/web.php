<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Business;

use App\BusinessUser;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/getPrize', 'HomeController@getPrize')->name('getPrize');
Route::get('/profile/', 'ProfileController@view')->name('profile');
Route::get('/business/',function (){
   //  $business_user=BusinessUser::select()->where('id_user',Auth::id())->get();
    $business_user=DB::select('select * from business_users where id_user = ?', array(Auth::id()));
     $countBusiness = Business::select('id')->count();
     $countBusinessUser= BusinessUser::select('id')->where('id_user', Auth::user()->id)->count();

    return view('business.business')->with(
        ['business_user'=>$business_user,'countBusiness'=>$countBusiness,'countBusinessUser'=>$countBusinessUser

        ]);
});

Route::get('/business/buy', 'BusinessController@buy');


Route::get('/business/{id}/detalies/', 'BusinessController@detalies')->name('BusinessDetalies');
Route::get('/business/{id}/upgrade/', 'BusinessController@upgrade')->name('BusinessUpgrade');
Route::get('/business/getProfit/', 'BusinessController@getProfit');
Route::get('/business/{id}/hire_workers/', 'BusinessController@hireWorkers')->name('BusinessHireWorkers');






