@extends('layouts.app')


@section('content')
<div class="container">
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading "> <a href="{{url('business')}}">Мои бизнесы </a></div>

                <div class="panel-body">

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif


                        <div class="panel-heading"> <a href="{{route('profile')}}">Мой профиль </a></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
