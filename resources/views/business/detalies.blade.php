@extends('layouts.app')
<?php
$business = new \App\Http\Controllers\BusinessController();
?>
@section('detalies')
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">
                <div class="panel-body">

                    <div class="center-block text-center"> <img src="{{ asset('images/business/'.$business_user->id_business.'.jpg') }}" width="50%"></div>
                    <div class="panel-heading"> {{$business_user->name}}, {{$business_user->level}}ур.</div>
                    <div class="panel-heading"> Прибыль: {{$business_user->profit}}, работники {{$business_user->workers}} из {{$business_user->max_workers}}</div>
                    <div class="panel-heading text-center"><span style="float:left" class ="text-left"><a href="{{route('BusinessHireWorkers',['id'=>$business_user->id])}} ">Нанять работника за {{$business->priceHireWorkers[$business_user->level]}}</a> </span>

                        <span  style="float:right" class ="text-right"> <a href="{{route('BusinessUpgrade',['id'=>$business_user->id])}} ">Улучшить бизнес за {{$business->priceUpgradeBusiness[$business_user->level]}}</a></span> </div>

                </div>
                    <div class="panel-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"><a href="{{url('/business')}}">Вернуться назад</a> </div></div>

                <div class="panel panel-default">
                            <div class="panel-heading"><a href="{{url('/home')}}"> На главную</a> </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
