@extends('layouts.app')
<?php
use App\Level;
$needExp = Level::select('experience')->where('id',Auth::user()->level)->first();

?>

@section('profile')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">


                    <div class="panel-body">

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                            <div class="panel-heading">Опыт: {{Auth::user()->experience}} из {{$needExp->experience }}</div>
                            <div class="panel-heading">Уровень: {{Auth::user()->level}}</div>
                            <div class="panel-heading">Баксы: {{Auth::user()->money}}</div>

                    </div>

                </div>
                <div class="container">
                    <div class="col-md-8 col-md-offset-2"><a href="{{url('/home')}}"> На главную</a> </div></div>

            </div>
        </div>
    </div>
@endsection